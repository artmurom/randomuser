package ru.artemtsarev.randomuser.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Name extends RealmObject implements Serializable {

    @PrimaryKey
    private String mId = UUID.randomUUID().toString();

    @SerializedName("title")
    private String mTitle;

    @SerializedName("first")
    private String mFirst;

    @SerializedName("last")
    private String mLast;

    public String getTitle() {
        return mTitle;
    }

    public String getFirst() {
        return mFirst;
    }

    public String getLast() {
        return mLast;
    }
}
