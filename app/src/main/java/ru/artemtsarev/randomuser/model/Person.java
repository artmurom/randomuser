package ru.artemtsarev.randomuser.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Person extends RealmObject implements Serializable {

    @PrimaryKey
    private String mId = UUID.randomUUID().toString();

    @SerializedName("name")
    private Name mName;

    @SerializedName("location")
    private Location mLocation;

    @SerializedName("picture")
    private Picture mPicture;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("phone")
    private String mPhone;

    @SerializedName("nat")
    private String mNat;

    public String getId() {
        return mId;
    }

    public Name getName() {
        return mName;
    }

    public Location getLocation() {
        return mLocation;
    }

    public Picture getPicture() {
        return mPicture;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getNat() {
        return mNat;
    }

    public void setId(String id) {
        mId = id;
    }
}
