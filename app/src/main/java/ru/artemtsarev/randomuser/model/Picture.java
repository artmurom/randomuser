package ru.artemtsarev.randomuser.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Picture extends RealmObject implements Serializable {

    @PrimaryKey
    private String mId = UUID.randomUUID().toString();

    @SerializedName("large")
    private String mLarge;

    @SerializedName("medium")
    private String mMedium;

    @SerializedName("thumbnail")
    private String mThumbnail;

    public String getLarge() {
        return mLarge;
    }

    public String getMedium() {
        return mMedium;
    }

    public String getThumbnail() {
        return mThumbnail;
    }


}
