package ru.artemtsarev.randomuser.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Result extends RealmObject implements Serializable {

    @PrimaryKey
    private String mId = UUID.randomUUID().toString();

    @SerializedName("results")
    private RealmList<Person> mPerson;

    /**
     * 0 - data from server
     * 1 - data from cache. Error update.
     */
    private int mDataFrom;

    public String getId() {
        return mId;
    }

    public RealmList<Person> getPerson() {
        return mPerson;
    }

    public int getDataFrom() {
        return mDataFrom;
    }

    public void setDataFrom(int dataFrom) {
        mDataFrom = dataFrom;
    }

}
