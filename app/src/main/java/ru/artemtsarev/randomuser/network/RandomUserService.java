package ru.artemtsarev.randomuser.network;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.artemtsarev.randomuser.model.Result;
import rx.Observable;


public interface RandomUserService {

    @GET("api/")
    Observable<Result> randomUsers(@Query("results") int results);

}
