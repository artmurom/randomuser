package ru.artemtsarev.randomuser.repository;

import android.support.annotation.NonNull;

import io.realm.Realm;
import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.model.Result;
import ru.artemtsarev.randomuser.network.ApiFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class DefaultRandomRepository implements RandomUserRepository {


    @NonNull
    @Override
    public Observable<Result> randomUsers(int results) {
        return ApiFactory.getRandomUserService()
                .randomUsers(results)
                .flatMap(response -> {

                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Result.class);
                        response.setDataFrom(0);
                        realm.insert(response);
                    });
                    return Observable.just(response);

                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    Result result = realm.where(Result.class).findAll().first();
                    Result res = realm.copyFromRealm(result);
                    res.setDataFrom(1);
                    return Observable.just(res);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Person getPerson(@NonNull String id) {
        Realm realm = Realm.getDefaultInstance();
        Person person = realm.where(Person.class).equalTo("mId", id).findFirst();
        return person;
    }
}
