package ru.artemtsarev.randomuser.repository;

import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.model.Result;
import rx.Observable;

public interface RandomUserRepository {

    @NonNull
    Observable<Result> randomUsers(int results);

    @NonNull
    Person getPerson(@NonNull String id);

}
