package ru.artemtsarev.randomuser.repository;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public final class RepositoryProvider {

    private static RandomUserRepository sRandomUserRepository;

    private RepositoryProvider() {
    }

    @NonNull
    public static RandomUserRepository provideRepository() {
        if (sRandomUserRepository == null) {
            sRandomUserRepository = new DefaultRandomRepository();
        }
        return sRandomUserRepository;
    }

    public static void setRepository(@NonNull RandomUserRepository randomUserRepository) {
        sRandomUserRepository = randomUserRepository;
    }

    @MainThread
    public static void init() {
        sRandomUserRepository = new DefaultRandomRepository();
    }
}
