package ru.artemtsarev.randomuser.screen.main;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artemtsarev.randomuser.R;
import ru.artemtsarev.randomuser.model.Result;
import ru.artemtsarev.randomuser.screen.person.PersonActivity;

public class MainActivity extends AppCompatActivity implements MainView, MainAdapter.OnItemClick, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;


    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private MainPresenter mMainPresenter;
    private MainAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MainAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));


        mMainPresenter = new MainPresenter(this);
        mMainPresenter.init();

        mSwipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void showUsers(@NonNull Result result) {
        if (result.getDataFrom() == 0) {
            showSnackBarMessage(getString(R.string.main_data_updeted));
        } else {
            showError();
        }
        mAdapter.changeDataSet(result.getPerson());
    }

    @Override
    public void showError() {
        showSnackBarMessage(getString(R.string.main_data_updeted_error));
    }

    public void showSnackBarMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        mMainPresenter.init();
        new Handler().postDelayed(() ->
                        mSwipeRefreshLayout.setRefreshing(false)
                , 1000);
    }

    @Override
    public void onItemClick(@NonNull String id) {
        startActivity(PersonActivity.makeIntent(this, id));
    }
}
