package ru.artemtsarev.randomuser.screen.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.artemtsarev.randomuser.R;
import ru.artemtsarev.randomuser.model.Person;


class MainAdapter extends RecyclerView.Adapter<MainHolder> {

    private final List<Person> mPersons;
    private final OnItemClick mOnItemClick;

    private final View.OnClickListener mInternalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Person person = (Person) view.getTag();
            mOnItemClick.onItemClick(person.getId());
        }
    };

    MainAdapter(@NonNull OnItemClick onItemClick) {
        mPersons = new ArrayList<>();
        mOnItemClick = onItemClick;

    }

    void changeDataSet(@NonNull List<Person> persons) {
        mPersons.clear();
        mPersons.addAll(persons);
        notifyDataSetChanged();
        notifyItemChanged(1);
    }

    @Override
    public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_person, parent, false);
        return new MainHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainHolder holder, int position) {
        Person person = mPersons.get(position);
        holder.bind(person);
        holder.itemView.setTag(person);
        holder.itemView.setOnClickListener(mInternalListener);
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    interface OnItemClick {

        void onItemClick(@NonNull String id);

    }
}
