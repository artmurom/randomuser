package ru.artemtsarev.randomuser.screen.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artemtsarev.randomuser.R;
import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.screen.utils.Images;


class MainHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.llPersonItem)
    LinearLayout mLlPersonItem;

    @BindView(R.id.ivPicture)
    ImageView mIvPicture;

    @BindView(R.id.tvFirstName)
    TextView mTvFirstName;

    @BindView(R.id.tvLastName)
    TextView mTvLastName;


    MainHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bind(@NonNull Person person) {
        Images.loadImage(mIvPicture, person.getPicture().getLarge());
        mTvFirstName.setText(person.getName().getFirst());
        mTvLastName.setText(person.getName().getLast());

    }
}
