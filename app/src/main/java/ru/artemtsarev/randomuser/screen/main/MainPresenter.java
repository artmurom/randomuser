package ru.artemtsarev.randomuser.screen.main;

import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.repository.RepositoryProvider;


public class MainPresenter {

    private final MainView mMainView;

    public MainPresenter(@NonNull MainView mainView) {
        mMainView = mainView;
    }

    public void init() {
        RepositoryProvider.provideRepository()
                .randomUsers(150)
                .subscribe(mMainView::showUsers, throwable -> mMainView.showError());
    }

}
