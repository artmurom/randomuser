package ru.artemtsarev.randomuser.screen.main;


import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.model.Result;

public interface MainView {

    void showUsers(@NonNull Result result);

    void showError();

}