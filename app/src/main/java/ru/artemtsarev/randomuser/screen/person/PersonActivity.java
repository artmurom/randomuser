package ru.artemtsarev.randomuser.screen.person;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.artemtsarev.randomuser.R;
import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.screen.utils.Images;

public class PersonActivity extends AppCompatActivity implements PersonView {

    private static final String ID_KEY = "id";

    @BindView(R.id.ivPicture)
    ImageView mIvPicture;

    @BindView(R.id.tvName)
    TextView mTvName;

    @BindView(R.id.tvEmail)
    TextView mTvEmail;

    @BindView(R.id.tvPhone)
    TextView mTvPhone;

    @BindView(R.id.tvNationality)
    TextView mTvNationality;

    @BindView(R.id.tvState)
    TextView mTvState;

    @BindView(R.id.tvCity)
    TextView mTvCity;

    @BindView(R.id.tvStreet)
    TextView mTvStreet;

    @BindView(R.id.btnShare)
    Button mBtnShare;

    private ActionBar toolbar;
    private PersonPresenter mPersonPresenter;
    private String mId, picture, name, email, phone, nat, state, city, street;


    @NonNull
    public static Intent makeIntent(@NonNull Activity activity, @NonNull String id) {
        Intent intent = new Intent(activity, PersonActivity.class);
        intent.putExtra(ID_KEY, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_card);
        ButterKnife.bind(this);

        mId = getIntent().getStringExtra(ID_KEY);

        toolbar = getSupportActionBar();

        mPersonPresenter = new PersonPresenter(this);
        mPersonPresenter.getPerson(mId);

        mBtnShare.setOnClickListener(view -> {
            mPersonPresenter.shareInfo();
        });
    }

    @Override
    public void initInterface(@NonNull Person person) {
        picture = person.getPicture().getLarge();
        name = person.getName().getFirst() + " " + person.getName().getLast();
        email = person.getEmail();
        phone = person.getPhone();
        nat = person.getNat();
        state = person.getLocation().getState();
        city = person.getLocation().getCity();
        street = person.getLocation().getStreet();

        Images.loadImage(mIvPicture, picture);
        mTvName.setText(name);
        mTvEmail.setText(email);
        mTvPhone.setText(phone);
        mTvNationality.setText(nat);
        mTvState.setText(state);
        mTvCity.setText(city);
        mTvStreet.setText(street);

        toolbar.setTitle(name);
        mTvPhone.setPaintFlags(mTvPhone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvPhone.setOnClickListener((v -> {
            mPersonPresenter.makeCall();
        }));
    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public void shareInfo() {
        String text = getString(R.string.person_common) + "\n" +
                getString(R.string.person_name) + " " + name + "\n" +
                getString(R.string.person_nationality) + " " + nat + "\n" +
                getString(R.string.person_contacts) + "\n" +
                getString(R.string.person_email) + " " + email + "\n" +
                getString(R.string.person_phone) + " " + phone + "\n" +
                getString(R.string.person_location) + "\n" +
                getString(R.string.person_state) + " " + state + "\n" +
                getString(R.string.person_city) + " " + city + "\n" +
                getString(R.string.person_street) + " " + street;

        Picasso.with(getApplicationContext()).load(picture).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, name);
                i.putExtra(Intent.EXTRA_TEXT, text);
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                startActivity(Intent.createChooser(i, getString(R.string.person_send_email)));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    @Override
    public void makeCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }
}
