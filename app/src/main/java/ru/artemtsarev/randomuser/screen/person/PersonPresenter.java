package ru.artemtsarev.randomuser.screen.person;

import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.repository.RepositoryProvider;


public class PersonPresenter {

    private final PersonView mPersonView;

    public PersonPresenter(@NonNull PersonView personView) {
        mPersonView = personView;
    }

    public void getPerson(@NonNull String id) {
        Person person = RepositoryProvider.provideRepository()
                .getPerson(id);
        mPersonView.initInterface(person);
    }

    public void shareInfo() {
        mPersonView.shareInfo();
    }

    public void makeCall() {
        mPersonView.makeCall();
    }
}
