package ru.artemtsarev.randomuser.screen.person;


import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.model.Person;

public interface PersonView {

    void initInterface(@NonNull Person person);

    void shareInfo();

    void makeCall();

}