package ru.artemtsarev.randomuser.screen.utils;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public final class Images {

    private Images() {
    }

    public static void loadImage(@NonNull ImageView imageView, @NonNull String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .transform(new CircleTransform())
                .into(imageView);
    }

}
