package ru.artemtsarev.randomuser;

import android.support.annotation.NonNull;

import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.model.Result;
import ru.artemtsarev.randomuser.repository.RandomUserRepository;
import rx.Observable;

public class TestRandomUserRepository implements RandomUserRepository {

    @NonNull
    @Override
    public Observable<Result> randomUsers(int results) {
        return Observable.empty();
    }

    @NonNull
    @Override
    public Person getPerson(@NonNull String id) {
        return null;
    }
}
