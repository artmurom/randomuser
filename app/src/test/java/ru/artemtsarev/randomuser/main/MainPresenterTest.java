package ru.artemtsarev.randomuser.main;

import android.support.annotation.NonNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.io.IOException;

import ru.artemtsarev.randomuser.TestRandomUserRepository;
import ru.artemtsarev.randomuser.model.Result;
import ru.artemtsarev.randomuser.repository.RandomUserRepository;
import ru.artemtsarev.randomuser.repository.RepositoryProvider;
import ru.artemtsarev.randomuser.screen.main.MainPresenter;
import ru.artemtsarev.randomuser.screen.main.MainView;
import rx.Observable;

import static junit.framework.Assert.assertNotNull;


@RunWith(JUnit4.class)
public class MainPresenterTest {

    private MainView mMainView;
    private MainPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        mMainView = Mockito.mock(MainView.class);

        mPresenter = new MainPresenter(mMainView);
    }

    @Test
    public void testCreated() throws Exception {
        assertNotNull(mPresenter);
        Mockito.verifyNoMoreInteractions(mMainView);
    }

    @Test
    public void testRepositoriesLoaded() throws Exception {
        Result result = new Result();
        RandomUserRepository repository = new TestRepository(result, false);
        RepositoryProvider.setRepository(repository);

        mPresenter.init();
        Mockito.verify(mMainView).showUsers(result);
    }

    @Test
    public void testRepositoriesLoadedError() throws Exception {
        Result result = new Result();
        RandomUserRepository repository = new TestRepository(result, true);
        RepositoryProvider.setRepository(repository);

        mPresenter.init();
        Mockito.verify(mMainView).showError();
    }

    @SuppressWarnings("ConstantConditions")
    @After
    public void tearDown() throws Exception {
        RepositoryProvider.setRepository(null);
    }


    private class TestRepository extends TestRandomUserRepository {

        private final Result mResult;
        private final boolean mError;

        public TestRepository(@NonNull Result result, boolean error) {
            mResult = result;
            mError = error;
        }

        @NonNull
        @Override
        public Observable<Result> randomUsers(int results) {
            if (mError) {
                return Observable.error(new IOException());

            }
            return Observable.just(mResult);
        }
    }


}
