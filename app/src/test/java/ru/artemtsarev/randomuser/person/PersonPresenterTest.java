package ru.artemtsarev.randomuser.person;

import android.support.annotation.NonNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import ru.artemtsarev.randomuser.TestRandomUserRepository;
import ru.artemtsarev.randomuser.model.Person;
import ru.artemtsarev.randomuser.model.Result;
import ru.artemtsarev.randomuser.repository.RandomUserRepository;
import ru.artemtsarev.randomuser.repository.RepositoryProvider;
import ru.artemtsarev.randomuser.screen.person.PersonPresenter;
import ru.artemtsarev.randomuser.screen.person.PersonView;

import static junit.framework.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class PersonPresenterTest {

    private PersonView mView;
    private PersonPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        mView = Mockito.mock(PersonView.class);

        mPresenter = new PersonPresenter(mView);
    }

    @Test
    public void testCreated() throws Exception {
        assertNotNull(mPresenter);
        Mockito.verifyNoMoreInteractions(mView);
    }

    @Test
    public void testGetPerson() throws Exception {
        Person person = new Person();
        person.setId("1");
        RandomUserRepository repository = new TestRepository(person);
        RepositoryProvider.setRepository(repository);

        mPresenter.getPerson("1");
        Mockito.verify(mView).initInterface(person);
    }

    @Test
    public void testShareInfoButtonClick() throws Exception {
        mPresenter.shareInfo();
        Mockito.verify(mView).shareInfo();
    }

    @Test
    public void testMakeCallButtonClick() throws Exception {
        mPresenter.makeCall();
        Mockito.verify(mView).makeCall();
    }


    @SuppressWarnings("ConstantConditions")
    @After
    public void tearDown() throws Exception {
        RepositoryProvider.setRepository(null);
    }


    private class TestRepository extends TestRandomUserRepository {

        private final Person mPerson;

        public TestRepository(@NonNull Person person) {
            mPerson = person;
        }

        @NonNull
        @Override
        public Person getPerson(@NonNull String id) {
            return mPerson;
        }
    }


}
